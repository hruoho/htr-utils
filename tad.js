/******************************************************
/*
/* Generate tests from JSDoc @examples
/*
/******************************************************/
const fs = require('fs')
const handlebars = require('handlebars')
const jsdoc = require('jsdoc-api')
const path = require('path')
const standard = require('standard')
const util = require('util')

/** @module tad */

// https://stackoverflow.com/a/18965152/4047675
handlebars.registerHelper('ifCond', function (v1, v2, options) {
  if (v1 === v2) {
    return options.fn(this)
  }
  return options.inverse(this)
})

const argv = require('yargs')
  .describe('t', 'template to use')
  .describe('b', 'basepath for modules')
  .describe('tbpath', 'path to templates')
  .alias('t', 'templ')
  .alias('b', 'bpath')
  .alias('o', 'output')
  .alias('v', 'version')
  .help('h')
  .usage('$0 --t templ-rewire.hb --b .. -o test index.js')
  .argv

const files = argv._
const templateBasePath = argv.tbpath || './test/templates'
const templ = path.join(templateBasePath, argv.templ || './templ.hb')
const bpath = argv.bpath || '.' // base path to modules
const outputDir = argv.o

if (!files) throw Error('No source files given')

// main
files.forEach(file => populateTemplate(file))

/**
* Populate a handlebars template
*/
function populateTemplate (file) {
  const parsed = jsdoc.explainSync({files: [file]})
    .filter(doclet => doclet.examples)
    .map(parseDoclet)

  const template = fs.readFileSync(templ, 'utf8')

  const compiled = handlebars.compile(template, { noEscape: true })

  let str = compiled({
    tests: parsed,
    file: path.join(bpath, file),
    moduleName: path.basename(file, '.js')
  })

  return letout(str, file)
}

/**
* Write linted test script to file or stdout
*/
function letout (str, file) {
  const outPath = outputDir ? path.join(outputDir, path.basename(file, '.js') + '.examples.js') : null
  const lint = util.promisify((str, cb) => standard.lintText(str, {fix: true}, cb))
  const writeFile = util.promisify(fs.writeFile)

  return lint(str)
    .then(results => {
      var linted = results.results[0].output
      if (!outputDir) return console.log(linted)
      return writeFile(outPath, linted)
    })
    .then(() => console.log('wrote', outPath))
    .catch(err => console.log(err))
}

/**
* Turn doclet into a testable object
* @returns {object} keys tname, tplan, examples
*/
function parseDoclet (doclet) {
  var tname = doclet.name
  var tplan = getTestPlan(doclet)
  let types = getTypes(doclet).join('|')
  var examples = doclet.examples.map(item => {
    let [desc, strExpr, strVal] = splitExample(item)
    let args = strExpr.replace(doclet.name, '') // remove function call
    return {strExpr, strVal, args, desc}
  })
  return {tname, tplan, fname: doclet.name, examples, types}
}

/**
* Predict test count
* @returns {number}
*/
function getTestPlan (doclet) {
  var exl = doclet.examples.length
  var rel = Array.isArray(doclet.returns) ? exl : 0
  return exl + rel
}

/**
* Combine doclet `@return`-values to a flat list
* @returns {Array}
*/
function getTypes (doclet) {
  if (doclet.returns) return doclet.returns.reduce((acc, cur) => acc.concat(cur.type.names), [])
}

/**
* Split single JSDoc example to array.
* @returns {Array}
*/
function splitExample (str) {
  let [example, strVal] = str.split('// =>').map(item => item.trim())
  if (example.indexOf('//') !== 0) return ['', example, strVal] // no description
  let [desc, strExpr] = example.split(/\n/)
  desc = desc.replace('//', '')
  return [desc, strExpr, strVal].map(item => item.trim().replace(/\n/, ' ', 'g'))
}
