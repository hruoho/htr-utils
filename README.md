[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)
# Time related utilities for Helsinki-Tallinna Race

## Requirements
* node 8 (eg. `utils.promisify` and es6 syntax)

## Tests
Most of the unit tests are generated from the JSDoc examples using `./tad.js`.
Update those tests with

    npm run tad

which does something along these lines:

    node tad.js --t templ-rewire.hb --b .. -o test index.js ./src/correction.js ./src/sailingTime.js

Then go

    npm test # equals `tap --cov test`

to see test overview and coverage.
Feel free to pass options for [tap](https://github.com/tapjs/node-tap), eg. `npm test -- -R spec`.

This way of generating tests from docs is inspired by [jsdoctest](https://www.npmjs.com/package/jsdoctest)
but the hand-cranked `tad.js` here utilizes jsdoc-api for parsing docs.
It might be turned to a separate module later on.

## JSDoc
Genereted documentation is not included in this repo. It can be created with

    npm run doc

which is more or less equal to this:

    jsdoc -t ./node_modules/ink-docstrap/template -R README.md -r index.js src/*.js
