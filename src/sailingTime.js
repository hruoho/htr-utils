const moment = require('moment')
require('moment-round')
require('moment-duration-format')

/** @module htr-utils/sailingTime */

module.exports = {
  finlys,
  generic,
  orc
}

/**
 * Score a finlys boat
 *
 * @param {object} opts
 * @param {string} opts.trueSailingTime (ISO date)
 * @param {number} opts.penalty penalty + compensation
 * @return {number} sailing time in ms
 * @example
 *   finlys({trueSailingTime: 1000*60*60*5})
 *     // => 1000*60*60*5
 * @example
 *   finlys({trueSailingTime: 1000*60*60*5, penalty: 10.5})
 *     // => 1000*60*60*5 + 1000*60*10.5
 * @example
 *   finlys({trueSailingTime: 1000*60*60*5, penalty: -10.5})
 *     // => 1000*60*60*5 - 1000*60*10.5
 */
function finlys ({trueSailingTime, penalty}) {
  return moment.duration(trueSailingTime, 'milliseconds')
    .add(penalty, 'minutes')
    .as('milliseconds')
}

/**
 * Score a non-finlys, non-orc boat (ie. business-lys or irc atm)
 * @param {object} opts
 * @param {number} opts.trueSailingTime ms
 * @param {number} opts.correction
 * @param {number} opts.penalty
 * @return {number} sailing time in ms
 * @example // irc
 *   generic({trueSailingTime: 1000*60*60*12, correction: 0, penalty: 0})
 *     // => 1000*60*60*12
 * @example // irc
 *   generic({trueSailingTime: 1000*60*60*12, correction: 2718, penalty: 0})
 *     // => 1000*60*60*12+1000*60*45+1000*18
 * @example // business
 *   generic({trueSailingTime: 1000*60*60*12, correction: 0, penalty: 0})
 *     // => 1000*60*60*12
 * @example // business
 *   generic({trueSailingTime: 1000*60*60*12, correction: 856, penalty: 0})
 *     // => 1000*60*60*12+1000*60*14+1000*16
 */
function generic ({trueSailingTime, correction, penalty}) {
  return moment.duration(trueSailingTime)
    .add(Math.round(correction), 'seconds')
    .add(parseFloat(penalty), 'minutes')
    .as('milliseconds')
}

/**
 * Score a ORC Club boat
 * @param {object} opts
 * @param {number} opts.trueSailingTime ms
 * @param {number} opts.correction ideal duration (=orcCorrection)
 * @param {number} opts.penalty
 * @return {number} An integer (ms), possibly less than zero
 * @example
 *   orc({trueSailingTime: 1000*60*60*12, correction: 24092, penalty: 0})
 *     // => 1000*60*60*12-1000*24092
 * @example
 *   orc({trueSailingTime: 1000*60*60*12, correction: 24092, penalty: 24092/60})
 *     // => 1000*60*60*12
 */
function orc ({trueSailingTime, correction, penalty}) {
  return moment.duration(trueSailingTime)
    .subtract(Math.round(correction), 'seconds')
    .add(parseFloat(penalty), 'minutes')
    .as('milliseconds')
}
