const VError = require('verror')
const moment = require('moment')
require('moment-round')
require('moment-duration-format')

/** @module htr-utils/correction */

module.exports = correction

/**
* Compute the corrections we need for sailingTimes.
*
* @return {number}
*
* @example // finlys
*   correction('finlys') // => 0
* @example // finlysVirtual
*   correction('finlysVirtual') // => 0
* @example // orc
*   correction('orc', {hcp: {orc_tod_off: 555.5}, minhcp: {orc_tod_off: 500}, route: 54.5}) // => 30275
* @example // irc
*   correction('irc', {hcp: {irc_tcc: 1.555}, minhcp: {irc_tcc: 1.0}, route: 54.5}) // => 11671
* @example // business lys
*   correction('business', {hcp: {finlys: 1.55}, minhcp: {finlys: 1.0}, route: 44.5}) // => 12080
*/
function correction (type, {hcp, minhcp, route} = {}) {
  if (type === 'finlys' || type === 'finlysVirtual') return 0

  const coefs = {
    irc: 600,
    default: 765 // business lys
  }

  switch (type) {
    case 'orc':
      return orc({hcp: hcp.orc_tod_off, route: route})
    case 'irc':
      return generic({hcp: hcp.irc_tcc, route: route, minhcp: minhcp.irc_tcc, coef: coefs.irc})
    default:
      return generic({hcp: hcp.finlys, route: route, minhcp: minhcp.finlys, coef: coefs.default})
  }
}

/**
 * Simple function to calculate ToD correction as seconds
 * @param {object} opts
 * @param {number} opts.coef    Coefficient (the constant related to race class type)
 * @param {number} opts.hcp     Handicap
 * @param {number} opts.minhcp  Smallest handicap
 * @param {number} opts.route   Route length in nm
 * @returns {number} Correction in seconds
 */
function generic ({coef, hcp, minhcp, route}) {
  if (!coef) throw VError('Coefficient missing or 0')
  if (!hcp) throw VError('Handicap missing or 0')
  if (!minhcp) throw VError('Minnum missing or zero!')
  if (!route) throw VError('Route length missing or 0')

  // NOTE in some earlier implementations minhcap was always 1.00
  const base = coef * route * ((1 - 1 / hcp) - (1 - 1 / minhcp))
  const dur = moment.duration(base, 'seconds').as('seconds')
  return Math.round(dur)
}

/**
 * Get correction for an ORC Club boat
 * @param {object} opts
 * @param {number} opts.hcp - orc tod offshore
 * @param {number} opts.route - route length in nm
 * @return Duration in seconds
 */
function orc ({hcp, route}) {
  if (!hcp) throw VError('Handicap missing or 0')
  if (!route) throw VError('Route length missing or 0')

  const base = parseFloat(hcp) * parseFloat(route)
  const dur = moment.duration(base, 'seconds').as('seconds')
  return Math.round(dur)
}
