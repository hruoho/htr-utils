const path = require('path')
const correction = require(path.join(__dirname, '/src/correction'))
const st = require('./src/sailingTime')
const moment = require('moment')
require('moment-round')

/** @module htr-utils */

module.exports = {
  avgSpeed,
  correctedFinishTime,
  sailingTime,
  startTime,
  trueSailingTime
}

/**
* Compute average speed.
* @param {object} opts
* @param {number} opts.trueSailingTime sailing time in ms
* @param {number} opts.route route length in nm
* @return {number}
* @example
*   avgSpeed({trueSailingTime: 1000*60*60*10, route: 50}) // => 5
* @example
*   avgSpeed({trueSailingTime: 1000*60*60*10, route: 100}) // => 10
*/
function avgSpeed ({trueSailingTime, route}) {
  return parseFloat(route) / moment.duration(trueSailingTime, 'milliseconds').as('hours')
}

/**
* Shift finishTime according to penalty/compensation.
* @param {object} opts
* @param {string} opts.finishTime datetime
* @param {number} opts.penalty minutes (penalties and compensation combined)
* @return {string} ISO Datetime
* @example
*   correctedFinishTime({finishTime: '2017-08-19T12:00:00', penalty: 50}) // => '2017-08-19T12:50:00Z'
* @example
*   correctedFinishTime({finishTime: '2017-08-19T12:00:00', penalty: 70.5}) // => '2017-08-19T13:10:30Z'
*/
function correctedFinishTime ({finishTime, penalty}) {
  return moment.utc(finishTime)
    .add(parseFloat(penalty), 'minutes')
    .format()
}

/**
* Compute comparable sailing time.
* @param {string} type finlys | finlysVirtual | irc | orc | any
* @param {object} opts
* @param {number} opts.trueSailingTime
* @param {string} opts.startTime Datetime,
* @param {string} opts.finishTime Datetime,
* @param {string} opts.first Datetime, earliest start time
* @param {object} opts.hcp handicaps
* @param {object} opts.minhcp minimized handicaps
* @param {number} opts.route route length in nm
* @param {number} opts.penalty penalty + compensation
* @param {number} opts.coef
* @return {number}
* @example
*   sailingTime('finlys', {trueSailingTime: 1000*60*60*5}) // => 1000*60*60*5
* @example
*   sailingTime('finlys', {trueSailingTime: 1000*60*60*5, penalty: 10.5})
*     // => 1000*60*60*5 + 1000*60*10.5
* @example
*   sailingTime('finlys', {trueSailingTime: 1000*60*60*5, penalty: -10.5})
*     // => 1000*60*60*5 - 1000*60*10.5
* @example
*   sailingTime('finlysVirtual', {startTime: '2017-08-18T18:00', first: '2017-08-18T18:00:00', finishTime: '2017-08-19T06:00:00'})
*     // => 1000*60*60*12
* @example
*   sailingTime('finlysVirtual', {startTime: '2017-08-18T18:00', first: '2017-08-18T18:00:00', finishTime: '2017-08-19T06:00:00', penalty: 30})
*     // => 1000*60*60*12.5
* @example
*   sailingTime('finlysVirtual', {startTime: '2017-08-18T18:00', first: '2017-08-18T18:00:00', finishTime: '2017-08-19T06:00:00', penalty: -30})
*     // => 1000*60*60*11.5
* @example
*   sailingTime('orc', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {orc_tod_off: 541.4}, minhcp: {orc_tod_off: 500.0}})
*     // => 1000*60*60*12-1000*24092
* @example
*   sailingTime('orc', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {orc_tod_off: 607.3}, minhcp: {orc_tod_off: 500.0}})
*     // => 1000*60*60*12-1000*27025
* @example
*   sailingTime('irc', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {irc_tcc: 1.1}, minhcp: {irc_tcc: 1.0}})
*     // => 1000*60*60*12+1000*60*40+1000*27
* @example
*   sailingTime('irc', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {irc_tcc: 1.31}, minhcp: {irc_tcc: 1.0}})
*     // => 1000*60*60*13+1000*60*45+1000*18
* @example
*   sailingTime('business', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {finlys: 1.1}, minhcp: {finlys: 1.0}})
*     // => 1000*60*60*12+1000*60*51+1000*35
* @example
*   sailingTime('business', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {finlys: 1.31}, minhcp: {finlys: 1.0}})
*     // => 1000*60*60*14+1000*60*14+1000*16
*/
function sailingTime (type, opts) {
  // copy opts to avoid mutation
  let myopts = Object.assign({}, opts)

  myopts.correction = correction(type, opts)

  if (!opts.trueSailingTime) {
    myopts.trueSailingTime = trueSailingTime(opts)
  }

  switch (type) {
    case 'finlysVirtual':
      myopts.trueSailingTime = trueSailingTime({startTime: opts.first, finishTime: opts.finishTime})
      // falls through
    case 'finlys':
      return st.finlys(myopts)
    case 'orc':
      return st.orc(myopts)
    default: // business lys | irc
      return st.generic(myopts)
  }
}

/**
* Compute (finlys) startTime.
* @param {string} type finlys
* @param {object} opts
* @param {string} opts.first datetime
* @param {number} opts.hcp  handicap
* @param {number} opts.minhcp smallest handicap
* @param {number} opts.route routel length in nm
* @return {string}
* @example
*   startTime('finlys', {first: '2017-08-18T18:00:00', hcp: 1.0, minhcp: 1.0, route: 44.5}) // => '2017-08-18T18:00:00.000Z'
* @example
*   startTime('finlys', {first: '2017-08-18T18:00:00', hcp: 1.2, minhcp: 1.0, route: 44.5}) // => '2017-08-18T19:34:30.000Z'
*/
function startTime (type, {first, hcp, minhcp, route}) {
  switch (type) {
    case 'finlys':
    default:
      // let diff = 765 * route * ((1 - 1 / hcp) - (1 - 1 / minhcp))
      const diff = 765 * route * (1 / minhcp - 1 / hcp)
      return moment.utc(first).add(diff, 'seconds').round(30, 'seconds').toISOString()
  }
}

/**
* Compute true sailing time.
* @param {object} opts
* @param {string} opts.startTime datetime
* @param {string} opts.finishTime datetime
* @return {number}
* @example
*   trueSailingTime({startTime: '2017-08-18T18:00:00', finishTime: '2017-08-18T18:00:00'}) // => 0
* @example
*   trueSailingTime({startTime: '2017-08-18T18:00:00', finishTime: '2017-08-19T06:00:00'}) // => 1000*60*60*12
*/
function trueSailingTime ({startTime, finishTime}) {
  if (!startTime || !finishTime) throw Error('missing params')
  return moment.utc(finishTime).diff(moment.utc(startTime))
}
