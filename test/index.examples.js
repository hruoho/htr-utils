const tap = require('tap')
const rewire = require('rewire')
var index = rewire('../index.js')

tap.test('avgSpeed', function (tap) {
  var matcher = new RegExp('number')
  var avgSpeed = index.__get__('avgSpeed')

  tap.equal(avgSpeed({trueSailingTime: 1000 * 60 * 60 * 10, route: 50}), 5)
  tap.match(typeof avgSpeed({trueSailingTime: 1000 * 60 * 60 * 10, route: 50}), matcher, 'typecheck')

  tap.equal(avgSpeed({trueSailingTime: 1000 * 60 * 60 * 10, route: 100}), 10)
  tap.match(typeof avgSpeed({trueSailingTime: 1000 * 60 * 60 * 10, route: 100}), matcher, 'typecheck')

  tap.end()
})
tap.test('correctedFinishTime', function (tap) {
  var matcher = new RegExp('string')
  var correctedFinishTime = index.__get__('correctedFinishTime')

  tap.equal(correctedFinishTime({finishTime: '2017-08-19T12:00:00', penalty: 50}), '2017-08-19T12:50:00Z')
  tap.match(typeof correctedFinishTime({finishTime: '2017-08-19T12:00:00', penalty: 50}), matcher, 'typecheck')

  tap.equal(correctedFinishTime({finishTime: '2017-08-19T12:00:00', penalty: 70.5}), '2017-08-19T13:10:30Z')
  tap.match(typeof correctedFinishTime({finishTime: '2017-08-19T12:00:00', penalty: 70.5}), matcher, 'typecheck')

  tap.end()
})
tap.test('sailingTime', function (tap) {
  var matcher = new RegExp('number')
  var sailingTime = index.__get__('sailingTime')

  tap.equal(sailingTime('finlys', {trueSailingTime: 1000 * 60 * 60 * 5}), 1000 * 60 * 60 * 5)
  tap.match(typeof sailingTime('finlys', {trueSailingTime: 1000 * 60 * 60 * 5}), matcher, 'typecheck')

  tap.equal(sailingTime('finlys', {trueSailingTime: 1000 * 60 * 60 * 5, penalty: 10.5}), 1000 * 60 * 60 * 5 + 1000 * 60 * 10.5)
  tap.match(typeof sailingTime('finlys', {trueSailingTime: 1000 * 60 * 60 * 5, penalty: 10.5}), matcher, 'typecheck')

  tap.equal(sailingTime('finlys', {trueSailingTime: 1000 * 60 * 60 * 5, penalty: -10.5}), 1000 * 60 * 60 * 5 - 1000 * 60 * 10.5)
  tap.match(typeof sailingTime('finlys', {trueSailingTime: 1000 * 60 * 60 * 5, penalty: -10.5}), matcher, 'typecheck')

  tap.equal(sailingTime('finlysVirtual', {startTime: '2017-08-18T18:00', first: '2017-08-18T18:00:00', finishTime: '2017-08-19T06:00:00'}), 1000 * 60 * 60 * 12)
  tap.match(typeof sailingTime('finlysVirtual', {startTime: '2017-08-18T18:00', first: '2017-08-18T18:00:00', finishTime: '2017-08-19T06:00:00'}), matcher, 'typecheck')

  tap.equal(sailingTime('finlysVirtual', {startTime: '2017-08-18T18:00', first: '2017-08-18T18:00:00', finishTime: '2017-08-19T06:00:00', penalty: 30}), 1000 * 60 * 60 * 12.5)
  tap.match(typeof sailingTime('finlysVirtual', {startTime: '2017-08-18T18:00', first: '2017-08-18T18:00:00', finishTime: '2017-08-19T06:00:00', penalty: 30}), matcher, 'typecheck')

  tap.equal(sailingTime('finlysVirtual', {startTime: '2017-08-18T18:00', first: '2017-08-18T18:00:00', finishTime: '2017-08-19T06:00:00', penalty: -30}), 1000 * 60 * 60 * 11.5)
  tap.match(typeof sailingTime('finlysVirtual', {startTime: '2017-08-18T18:00', first: '2017-08-18T18:00:00', finishTime: '2017-08-19T06:00:00', penalty: -30}), matcher, 'typecheck')

  tap.equal(sailingTime('orc', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {orc_tod_off: 541.4}, minhcp: {orc_tod_off: 500.0}}), 1000 * 60 * 60 * 12 - 1000 * 24092)
  tap.match(typeof sailingTime('orc', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {orc_tod_off: 541.4}, minhcp: {orc_tod_off: 500.0}}), matcher, 'typecheck')

  tap.equal(sailingTime('orc', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {orc_tod_off: 607.3}, minhcp: {orc_tod_off: 500.0}}), 1000 * 60 * 60 * 12 - 1000 * 27025)
  tap.match(typeof sailingTime('orc', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {orc_tod_off: 607.3}, minhcp: {orc_tod_off: 500.0}}), matcher, 'typecheck')

  tap.equal(sailingTime('irc', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {irc_tcc: 1.1}, minhcp: {irc_tcc: 1.0}}), 1000 * 60 * 60 * 12 + 1000 * 60 * 40 + 1000 * 27)
  tap.match(typeof sailingTime('irc', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {irc_tcc: 1.1}, minhcp: {irc_tcc: 1.0}}), matcher, 'typecheck')

  tap.equal(sailingTime('irc', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {irc_tcc: 1.31}, minhcp: {irc_tcc: 1.0}}), 1000 * 60 * 60 * 13 + 1000 * 60 * 45 + 1000 * 18)
  tap.match(typeof sailingTime('irc', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {irc_tcc: 1.31}, minhcp: {irc_tcc: 1.0}}), matcher, 'typecheck')

  tap.equal(sailingTime('business', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {finlys: 1.1}, minhcp: {finlys: 1.0}}), 1000 * 60 * 60 * 12 + 1000 * 60 * 51 + 1000 * 35)
  tap.match(typeof sailingTime('business', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {finlys: 1.1}, minhcp: {finlys: 1.0}}), matcher, 'typecheck')

  tap.equal(sailingTime('business', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {finlys: 1.31}, minhcp: {finlys: 1.0}}), 1000 * 60 * 60 * 14 + 1000 * 60 * 14 + 1000 * 16)
  tap.match(typeof sailingTime('business', {startTime: '2017-08-18T18:00', finishTime: '2017-08-19T06:00:00', route: 44.5, hcp: {finlys: 1.31}, minhcp: {finlys: 1.0}}), matcher, 'typecheck')

  tap.end()
})
tap.test('startTime', function (tap) {
  var matcher = new RegExp('string')
  var startTime = index.__get__('startTime')

  tap.equal(startTime('finlys', {first: '2017-08-18T18:00:00', hcp: 1.0, minhcp: 1.0, route: 44.5}), '2017-08-18T18:00:00.000Z')
  tap.match(typeof startTime('finlys', {first: '2017-08-18T18:00:00', hcp: 1.0, minhcp: 1.0, route: 44.5}), matcher, 'typecheck')

  tap.equal(startTime('finlys', {first: '2017-08-18T18:00:00', hcp: 1.2, minhcp: 1.0, route: 44.5}), '2017-08-18T19:34:30.000Z')
  tap.match(typeof startTime('finlys', {first: '2017-08-18T18:00:00', hcp: 1.2, minhcp: 1.0, route: 44.5}), matcher, 'typecheck')

  tap.end()
})
tap.test('trueSailingTime', function (tap) {
  var matcher = new RegExp('number')
  var trueSailingTime = index.__get__('trueSailingTime')

  tap.equal(trueSailingTime({startTime: '2017-08-18T18:00:00', finishTime: '2017-08-18T18:00:00'}), 0)
  tap.match(typeof trueSailingTime({startTime: '2017-08-18T18:00:00', finishTime: '2017-08-18T18:00:00'}), matcher, 'typecheck')

  tap.equal(trueSailingTime({startTime: '2017-08-18T18:00:00', finishTime: '2017-08-19T06:00:00'}), 1000 * 60 * 60 * 12)
  tap.match(typeof trueSailingTime({startTime: '2017-08-18T18:00:00', finishTime: '2017-08-19T06:00:00'}), matcher, 'typecheck')

  tap.end()
})
