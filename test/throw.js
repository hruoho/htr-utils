const tap = require('tap')
const index = require('../index')
const rewire = require('rewire')
const correction = rewire('../src/correction')
const generic = correction.__get__('generic')
const orc = correction.__get__('orc')

tap.test('trueSailingTime throws', tap => {
  tap.plan(2)
  tap.throws(() => index.trueSailingTime({startTime: '', finishTime: ''}))
  tap.throws(() => index.trueSailingTime({startTime: '2017-08-18T18:00', finishTime: ''}))
})

tap.test('generic correction throws', tap => {
  tap.plan(4)
  tap.throws(() => generic({coef: 765, hcp: {finlys: 1.55}, minhcp: {finlys: 1.0}}))
  tap.throws(() => generic({coef: 765, hcp: {finlys: 1.55}, route: 44.5}))
  tap.throws(() => generic({coef: 765, minhcp: {finlys: 1.0}, route: 44.5}))
  tap.throws(() => generic({hcp: {finlys: 1.55}, minhcp: {finlys: 1.0}, route: 44.5}))
})

tap.test('orc correction throws', tap => {
  tap.plan(2)
  tap.throws(() => orc({hcp: {orc_tod_off: 555.5}}))
  tap.throws(() => orc({route: 54.5}))
})
