const tap = require('tap')
const rewire = require('rewire')
var correction = rewire('../src/correction.js')

tap.test('correction', function (tap) {
  var matcher = new RegExp('number')
  correction = correction.__get__('correction')

  tap.equal(correction('finlys'), 0, 'finlys')
  tap.match(typeof correction('finlys'), matcher, 'typecheck')

  tap.equal(correction('finlysVirtual'), 0, 'finlysVirtual')
  tap.match(typeof correction('finlysVirtual'), matcher, 'typecheck')

  tap.equal(correction('orc', {hcp: {orc_tod_off: 555.5}, minhcp: {orc_tod_off: 500}, route: 54.5}), 30275, 'orc')
  tap.match(typeof correction('orc', {hcp: {orc_tod_off: 555.5}, minhcp: {orc_tod_off: 500}, route: 54.5}), matcher, 'typecheck')

  tap.equal(correction('irc', {hcp: {irc_tcc: 1.555}, minhcp: {irc_tcc: 1.0}, route: 54.5}), 11671, 'irc')
  tap.match(typeof correction('irc', {hcp: {irc_tcc: 1.555}, minhcp: {irc_tcc: 1.0}, route: 54.5}), matcher, 'typecheck')

  tap.equal(correction('business', {hcp: {finlys: 1.55}, minhcp: {finlys: 1.0}, route: 44.5}), 12080, 'business lys')
  tap.match(typeof correction('business', {hcp: {finlys: 1.55}, minhcp: {finlys: 1.0}, route: 44.5}), matcher, 'typecheck')

  tap.end()
})
