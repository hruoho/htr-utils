const tap = require('tap')
const rewire = require('rewire')
var sailingTime = rewire('../src/sailingTime.js')

tap.test('finlys', function (tap) {
  var matcher = new RegExp('number')
  var finlys = sailingTime.__get__('finlys')

  tap.equal(finlys({trueSailingTime: 1000 * 60 * 60 * 5}), 1000 * 60 * 60 * 5)
  tap.match(typeof finlys({trueSailingTime: 1000 * 60 * 60 * 5}), matcher, 'typecheck')

  tap.equal(finlys({trueSailingTime: 1000 * 60 * 60 * 5, penalty: 10.5}), 1000 * 60 * 60 * 5 + 1000 * 60 * 10.5)
  tap.match(typeof finlys({trueSailingTime: 1000 * 60 * 60 * 5, penalty: 10.5}), matcher, 'typecheck')

  tap.equal(finlys({trueSailingTime: 1000 * 60 * 60 * 5, penalty: -10.5}), 1000 * 60 * 60 * 5 - 1000 * 60 * 10.5)
  tap.match(typeof finlys({trueSailingTime: 1000 * 60 * 60 * 5, penalty: -10.5}), matcher, 'typecheck')

  tap.end()
})
tap.test('generic', function (tap) {
  var matcher = new RegExp('number')
  var generic = sailingTime.__get__('generic')

  tap.equal(generic({trueSailingTime: 1000 * 60 * 60 * 12, correction: 0, penalty: 0}), 1000 * 60 * 60 * 12, 'irc')
  tap.match(typeof generic({trueSailingTime: 1000 * 60 * 60 * 12, correction: 0, penalty: 0}), matcher, 'typecheck')

  tap.equal(generic({trueSailingTime: 1000 * 60 * 60 * 12, correction: 2718, penalty: 0}), 1000 * 60 * 60 * 12 + 1000 * 60 * 45 + 1000 * 18, 'irc')
  tap.match(typeof generic({trueSailingTime: 1000 * 60 * 60 * 12, correction: 2718, penalty: 0}), matcher, 'typecheck')

  tap.equal(generic({trueSailingTime: 1000 * 60 * 60 * 12, correction: 0, penalty: 0}), 1000 * 60 * 60 * 12, 'business')
  tap.match(typeof generic({trueSailingTime: 1000 * 60 * 60 * 12, correction: 0, penalty: 0}), matcher, 'typecheck')

  tap.equal(generic({trueSailingTime: 1000 * 60 * 60 * 12, correction: 856, penalty: 0}), 1000 * 60 * 60 * 12 + 1000 * 60 * 14 + 1000 * 16, 'business')
  tap.match(typeof generic({trueSailingTime: 1000 * 60 * 60 * 12, correction: 856, penalty: 0}), matcher, 'typecheck')

  tap.end()
})
tap.test('orc', function (tap) {
  var matcher = new RegExp('number')
  var orc = sailingTime.__get__('orc')

  tap.equal(orc({trueSailingTime: 1000 * 60 * 60 * 12, correction: 24092, penalty: 0}), 1000 * 60 * 60 * 12 - 1000 * 24092)
  tap.match(typeof orc({trueSailingTime: 1000 * 60 * 60 * 12, correction: 24092, penalty: 0}), matcher, 'typecheck')

  tap.equal(orc({trueSailingTime: 1000 * 60 * 60 * 12, correction: 24092, penalty: 24092 / 60}), 1000 * 60 * 60 * 12)
  tap.match(typeof orc({trueSailingTime: 1000 * 60 * 60 * 12, correction: 24092, penalty: 24092 / 60}), matcher, 'typecheck')

  tap.end()
})
