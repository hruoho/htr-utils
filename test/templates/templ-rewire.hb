const tap = require('tap')
const rewire = require('rewire')
var {{moduleName}} = rewire('{{file}}')

{{#*inline "myTest"}}
tap.equal({{fname}}{{args}}, {{strVal}}{{#if desc}}, '{{desc}}'{{/if}})
tap.match(typeof {{fname}}{{args}}, matcher, 'typecheck')
{{/inline}}

{{#each tests}}
tap.test('{{tname}}', function (tap) {
  var matcher = new RegExp('{{types}}')
  {{#ifCond @root.moduleName fname}}
  {{fname}} = {{@root.moduleName}}.__get__('{{fname}}')
  {{else}}
  var {{fname}} = {{@root.moduleName}}.__get__('{{fname}}')
  {{/ifCond}}

  {{#each examples}}

  {{> myTest fname=../fname}}

  {{/each}}
  tap.end()
})
{{/each}}
