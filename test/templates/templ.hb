const tap = require('tap')
const {{moduleName}} = require('{{file}}')

{{#each tests}}
tap.test('{{tname}}', function (tap) {
  {{#each examples}}
  tap.equal({{@root.moduleName}}.{{this.[0]}}, {{this.[1]}})
  {{/each}}
  tap.end()
})


{{/each}}
